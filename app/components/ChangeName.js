var React = require('react');
var config = require('../configs/mainConfig');

var ChangeName = React.createClass({
	getInitialState: function () {
    return {
	      	name: ""
	    };
	},

	storeName: function(e){
		this.state.name = e.target.value;

	},

	deactivateEditName: function(e){
		if (e.target !== e.currentTarget){
			return;
		}
		this.props.deactivateEditName();
	},

	editName: function(){
		this.props.editName(this.state.name);
	},
	componentWillMount: function(){
		this.state.name = this.props.currentName;
	},
	render: function(){
		return (
				<div className="grayscreen" onClick={this.deactivateEditName}>
					<div className="popup">
						<span>Escribe un nuevo nombre para tu proyecto</span><br/>
						<img src={config.changeName.icon}/>
						<input className="new-name" type="text" name="newName" defaultValue={this.state.name || ''} onChange={this.storeName} maxLength={34}/>
						<input className="send button" type="submit" onClick={this.editName}/>
					</div>
				</div>
			);
	}
});

module.exports = ChangeName;