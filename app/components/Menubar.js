var React = require('react');
var config = require('../configs/mainConfig');

var graytone = {
	filter: "brightness(500%) grayscale(100%)"
};

var Menubar = React.createClass({
	render: function(){
		var menuOptions = [];
		switch(this.props.mode){
			case 0:
			menuOptions.push(
						<div key={0} className="menu-item" onClick = {this.props.createNewProject}>
							<div className="menu-image"><img src={config.menubar.newProject.active}/></div>
							<div className="menu-description">Añadir proyecto</div>
						</div>);
			if (this.props.selected){
				menuOptions.push(
						<div key={1} className="menu-item" onClick = {this.props.activateEditName.bind(null, true)}>
							<div className="menu-image"><img src={config.menubar.changeName.active}/></div>
							<div className="menu-description">Cambiar nombre</div>
						</div>
					)
				menuOptions.push(
						<div key={2} className="menu-item" onClick = {this.props.activateRemoveItem}>
							<div className="menu-image"><img src={config.menubar.remove.active}/></div>
							<div className="menu-description">Eliminar</div>
						</div>
					)
			}
			else{
				menuOptions.push(
						<div key={1} className="menu-item-inactive">
							<div className="menu-image"><img src={config.menubar.changeName.inactive}/></div>
							<div className="menu-description">Cambiar nombre</div>
						</div>
					)	
				menuOptions.push(
						<div key={2} className="menu-item-inactive">
							<div className="menu-image"><img src={config.menubar.remove.inactive}/></div>
							<div className="menu-description">Eliminar</div>
						</div>
					)	
			}
			break;
			case 1:
			menuOptions.push(
						<div key={0} className="menu-item-green" onClick = {this.props.uploadPhotos}>
							<div className="menu-image"><img src={config.menubar.upload.active}/></div>
							<div className="menu-description">Subir contenido</div>
						</div>);
			menuOptions.push(
						<div key={1} className="menu-item" onClick = {this.props.uploadTour}>
							<div className="menu-image"><img src={config.menubar.newTour.active}/></div>
							<div className="menu-description">Crear tour</div>
						</div>);
			if (this.props.selected){
				menuOptions.push(
						<div key={2} className="menu-item" onClick = {this.props.activateEditName.bind(null, true)}>
							<div className="menu-image"><img src={config.menubar.changeName.active}/></div>
							<div className="menu-description">Cambiar nombre</div>
						</div>
					)
				menuOptions.push(
						<div key={3} className="menu-item" onClick = {this.props.activateRemoveItem}>
							<div className="menu-image"><img src={config.menubar.remove.active}/></div>
							<div className="menu-description">Eliminar</div>
						</div>
					)
			}
			else{
				menuOptions.push(
						<div key={2} className="menu-item-inactive">
							<div className="menu-image"><img src={config.menubar.changeName.inactive}/></div>
							<div className="menu-description">Cambiar nombre</div>
						</div>
					)	
				menuOptions.push(
						<div key={3} className="menu-item-inactive">
							<div className="menu-image"><img src={config.menubar.remove.inactive}/></div>
							<div className="menu-description">Eliminar</div>
						</div>
					)	
			}

			break;


		}

		return (
				<div>
					<div id="mock-menubar"/>
					<div id="menubar">
						<div id="menubar-content">
							{menuOptions}
						</div>
						
					</div>
				</div>
			);
	}
});



module.exports = Menubar;