var React = require('react');
var RadialButtons = require('./RadialButtons');
var ApiGateway = require('../actions/ApiGateway');
var config = require('../configs/mainConfig');
var HelpFunctions = require('../actions/HelpFunctions');


var EditContent = React.createClass({
	getInitialState: function () {
    return {
	      	conf: {},
	      	vr: false,
	      	embedCode: "",
	      	videoProcessing: false,
	    };
	},

	componentWillMount: function(){
		if (this.props.cContent.type == "videos") ApiGateway.getPlayerConfig(this, this.props.user, this.props.cContent.uuid)
		if (this.props.cContent.vr === true) this.setState({vr: true})
		else this.setState({vr: false})
	},

	loadConf: function(conf){
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = this.checkConf.bind(null, xhr, conf);
		xhr.open("GET", conf.conf.source.dash, true);
		xhr.send();
		
	},

	checkConf: function(xhr, conf){
		if (xhr.readyState == 4) {
			var isVideoAvailable = false;
		    xml = HelpFunctions.xmlToJson(jQuery.parseXML(xhr.responseText));
		    if(!xml.hasOwnProperty("Error")) isVideoAvailable = true;  
		    if (!isVideoAvailable){
			}
			this.setState({conf: conf});
			if (isVideoAvailable) {
				this.applyConf(conf);
				this.setState({videoProcessing: false});
			}
			else{
				this.setState({videoProcessing: true});
			}
		}
	},

	applyConf: function(conf){
		player = bitmovin.player("player");
		player.setup(conf.conf).then(function(value) {
	        // Success
	        console.log("Successfully created bitmovin player instance");
		}, function(reason) {
	        // Error!
	        console.log("Error while creating bitmovin player instance");
		});
	},

	closeEditContent: function(e){
		if (e.target !== e.currentTarget){
			return;
		}
		if(player.hasOwnProperty("destroy")) player.destroy();
		this.props.closeEditContent();
	},

	updatePlayerConfig: function(){
		ApiGateway.updatePlayerConfig(this, this.props.user, this.props.cContent.uuid, this.state.vr);
		this.setState({embedCode: '<iframe width="560" height="315" src="http://www.libelios.com/embedv?user=' + this.props.user + '&cuuid=' + this.props.cContent.uuid + '" frameborder="0" allowfullscreen></iframe>'})
	},

	isItVideo360: function(value){
		if (value == 0){
			this.state.conf.conf.source.vr = {};
			this.setState({vr: true})
		}
		else if(value == 1){
			delete this.state.conf.conf.source.vr;
			this.setState({vr: false})
		}
		if(player.hasOwnProperty("destroy")) player.destroy();
		this.loadConf(this.state.conf)
	},

	render: function(){
		var defaultColor = ['ffffff', 'f8f9e1', 'f8eacc', 'f5e4ee', 'f1cce5', 'f2cecf', 'e0ced5', 'ecf3f0', 'cce9da', 'ccddd3', 'ecf5fc', 'e2e8f3', 'ccd2e5', 'eeeeee', 'cccccc']
		var defaultColorInput = [];
		var videoProcessing = this.state.videoProcessing?"Please wait, the video is being processed. This can take several minutes":"";
		for (var i = 0; i < defaultColor.length; i++){
			defaultColorInput.push(

				<div key={i} className="defaultColorContainer">
					<div className="defaultColor" style = {{backgroundColor: '#' + defaultColor[i]}}/>
				</div>
				)
		}

		return (
				<div className="grayscreenSub" onClick={this.closeEditContent}>
					<div className="editContent">
						<div className="editContentTop">
							<div className="editContentTitle">
								<div><span>{this.props.cContent.name}</span></div>
								<div><img src={config.confContent.editIcon} onClick = {this.props.activateEditName.bind(null, false)}/></div>
							</div>
							<div className="editContentClose">
								<span onClick = {this.closeEditContent}>✖</span>
							</div>

						</div>


						<div className="editContentMiddle">
							<div className="editContentMiddleLeft">
								<div className="editContentMediaContainer">
									<div className="editContentMediaPlayer">
										<div id="player">{videoProcessing}</div>
									</div>
									<div className="editContentMediaInfo">
									</div>
								</div>
							</div>
							<div className="editContentMiddleRight">
								<div className="editContentMiddleRightTopSpace"/>
								<div className="editContentSection">
									<div className="editContentNum">
										<span className="inactiveText">1.</span>
									</div>
									<div className="editContentDes">
										<div><span className="inactiveText">Añade un <span className="bold">icono</span> a tu video:</span></div>
										<div className="confDefaultButtons"><input className="posButtonIn" type="submit" value="Subir icono..."/><input className="negButtonIn" type="submit" value="Quitar icono"/></div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										<span className="inactiveText">2.</span>
									</div>
									<div className="editContentDes">
										<span className="inactiveText"> Elija el color para la barra del reproductor: </span>
										<br/>
										<div className="defaultColors">
											{defaultColorInput}
										</div>
										<div className="colorCode">
											# <input type="text" disabled/>
										</div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										<span className="inactiveText">3.</span>
									</div>
									<div className="editContentDes">
										<span className="inactiveText">Personaliza el thumbnail:</span>
										<div className="confDefaultButtons"><input className="posButtonIn" type="submit" value="Subir imagen..." disabled/><input className="negButtonIn" type="submit" value="Generar imagen" disabled/></div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										4.
									</div>
									<div className="editContentDes">
										Es un video <span className="bold">360</span> º?
										<div className="videoTypeSelection">
											<RadialButtons default = {this.props.cContent.vr === true?0:1} captions = {["Sí", "No"]} selectionChanged={this.isItVideo360}/>
										</div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										5.
									</div>
									<div className="editContentDes">
										Obten el <span className="bold">código embed</span>:
										<div className="embedGeneration">
											<div className="embedGenerationButton"><input className="corpButton" type="submit" value="GENERAR" onClick={this.updatePlayerConfig}/></div>
											<div className="embedGenerationDesc">Genera el código embed y guarda<br/> la nueva configuración.</div>
										</div>
										<div className="embedGet">
											<input type="text" value={this.state.embedCode}/><input type="submit" value="copiar"/>
										</div>
									</div>		
								</div>
							</div>
						</div>
					</div>
				</div>
			);
	}
});

module.exports = EditContent;


