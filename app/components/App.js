var React = require('react');
var Header = require('./Header');
var Browser = require('./Browser');
var Login = require('./Login');
var ApiGateway = require('../actions/ApiGateway');
var CognitoManagment = require('../actions/CognitoManagment');
var HelpFunctions = require('../actions/HelpFunctions')


var App = React.createClass({
	getInitialState: function () {
    return {
	      	projects: [
	      	],
	      	content: [
	      	],
	      	mode : 5, //0: All projects  1:Project content  2:Upload pic/vid 4:Upload tour 5:Login
	      	cProject: {},
	      	currentProject: "",
	      	currentProjectUuid: "",
	      	cContent: {},
	      	currentContent: "",
	      	currentContentUuid: "",
	      	loading: false,
	      	user: "locorez",
	      	newProject: false,
	      	credentials: {},
	      	newContent: false,
	    };
	},

	componentWillMount: function (){
		var func = HelpFunctions.getQueryParameter("f");
		if (func === "v"){
			var username = HelpFunctions.getQueryParameter("u");
			console.log(username)
			var verificationCode = HelpFunctions.getQueryParameter("v");
			CognitoManagment.confirmUser(username,verificationCode);
		}

		cognitoUser = CognitoManagment.getCurrentUser()
		if (cognitoUser != null) {
			this.state.user = cognitoUser.username;
			this.showAllProjects();
		}
		//this.checkSession();
	},

	loginUser: function(username, password, LoginComp){
		this.setState({loading: true});
		CognitoManagment.loginUser(username, password, this, LoginComp);
	},

	registerUser: function(email, username, password, LoginComp){
		this.setState({loading: true});
		CognitoManagment.registerUser(email, username, password, this, LoginComp);
	},

	sendFrgtPswVerificationCode: function(username, LoginComp){
		this.setState({loading: true});
		CognitoManagment.sendFrgtPswVerificationCode(username, this, LoginComp)
	},

	setNewPasswordUnauthenticated: function(username, verificationCode, newPassword, LoginComp){
		this.setState({loading: true});
		CognitoManagment.setNewPasswordUnauthenticated(username, verificationCode, newPassword, this, LoginComp)
	},

	setNewPasswordForNewUser: function(username, newPassword, LoginComp){
		this.setState({loading: true});
		CognitoManagment.setNewPasswordForNewUser(username, newPassword, this, LoginComp);
	},

	logoutUser: function(){
		CognitoManagment.logoutUser();
		this.showLoginScreen();
	},

	showLoginScreen: function(){
		this.setState({mode: 5});
	},

	checkSession: function(){
		cognitoUser = CognitoManagment.getCurrentUser();
		CognitoManagment.checkSessionValidity(cognitoUser, this)
	},

	

	showAllProjects: function(){
		console.log("LEL")
		this.checkSession()
		this.setState({loading: true})
		ApiGateway.getProject(this, this.state.user);
		this.setState({mode: 0})
		this.setState({newProject: false});
	},

	/// openProject
	/// changed to project mode and load all the content of the specified project
	/// id: project's list ID to be openen (put any number if using uuid/name)
	/// uuid: project's uuid to open (not needed if id is used)
	/// name: project's name to open (not needed if id is used)
	/// newContent: true if content configuration of new content has also to be opened (optional)

	openProject: function(id, project, newContent){ 
		this.checkSession()
		this.setState({newProject: false});
		this.setState({loading: true})
		if (project == null){
			ApiGateway.getContent(this, this.state.user, this.state.projects[id].uuid);
			this.setState({currentProject: this.state.projects[id].name});
			this.setState({currentProjectUuid: this.state.projects[id].uuid});
			this.setState({cProject: this.state.projects[id]})
		}
		else {
			ApiGateway.getContent(this, this.state.user, project.uuid);
			this.setState({currentProject: project.name});
			this.setState({currentProjectUuid: project.uuid});
			this.setState({cProject: project});
		}
		if (newContent != null && newContent === true){
			this.setState({newContent: true});
		}
		this.setState({mode: 1})
	},

	/// deactivateNewContent
	/// has to be called when contentConfig is closed for not opening it automatically everytime Browser updates

	deactivateNewContent: function(){
		this.setState({newContent: false});
	},

	/// createNewProject
	/// creates new project and shows it

	createNewProject: function(){
		this.checkSession()
		this.setState({loading: true})
		ApiGateway.createProject(this, this.state.user);
		
	},

	removeSelectedProject: function(selectedID){
		this.checkSession()
		this.setState({loading: true})
		this.setState({mode: 0});
		ApiGateway.deleteProject(this, this.state.user, this.state.projects[selectedID].uuid);
		
	},

	editSelectedProjectName: function(newName, selectedID){
		this.checkSession()
		this.state.loading = true;
		this.setState({mode: 0});
		ApiGateway.updateProjectName(this, this.state.user, this.state.projects[selectedID].uuid, newName);
	},

	editCurrentProjectName: function(newName){
		this.checkSession()
		this.setState({newProject: false});
		this.state.loading = true;
		ApiGateway.updateProjectName(this, this.state.user, this.state.cProject.uuid, newName);
		var updateProject = this.state.cProject;
		updateProject.name = newName;
		this.setState({cProject: updateProject});
	},

	createContent: function(name, type, files, projectUuid){
		ApiGateway.databaseCreate(this, this.state.user, projectUuid, name, type, files)
	},

	editSelectedContentName: function(newName, selectedID){
		this.checkSession()
		this.state.loading = true;
		this.setState({mode: 1});
		ApiGateway.updateContentName(this, this.state.user, this.state.cProject.uuid, this.state.content[selectedID].uuid, newName);
	},

	editCurrentContentName: function(newName){
		this.checkSession()
		this.state.loading = true;
		ApiGateway.updateContentName(this, this.state.user, this.state.cProject.uuid, this.state.cContent.uuid, newName);

		var updateContent = this.state.cContent;
		updateContent.name = newName;
		this.setState({cContent: updateContent});
		
	},

	removeSelectedContent: function(selectedID){
		this.checkSession()
		this.setState({loading: true})
		this.setState({mode: 1});
		ApiGateway.deleteContent(this, this.state.user, this.state.cProject.uuid, this.state.content[selectedID].uuid, this.state.content[selectedID].type);
		
	},

	uploadPhotos: function(){
		this.setState({loading: true})
		CognitoManagment.getCredentials(cognitoUser, this)
		this.setState({mode: 2});
	},

	uploadTour: function(){
		this.setState({loading: true})
		CognitoManagment.getCredentials(cognitoUser, this)
		this.setState({mode: 4});
	},

	loadingAfterUpload: function(){
		this.setState({loading: true});
		this.setState({mode: 1});
	},

	startPhotosConversion: function(){
		ApiGateway.photosConvert(this, this.state.user, this.state.cContent.uuid);
	},

	startVideoConversion: function(){
		ApiGateway.videosConvert(this, this.state.user, this.state.cContent.uuid);
	},

	setCurrentContent: function(id, item){
		if (item == null){
			this.setState({cContent: this.state.content[id]});
		}
		else {
			this.setState({cContent: item});
		}
	},


	render: function(){
		if (this.state.mode == 0){
			var items = this.state.projects;
		}
		if (this.state.mode == 1){
			var items = this.state.content;
		}


		var pageContent = [];

		if (this.state.mode == 5){
			pageContent = 
			<Login 
			loginUser = {this.loginUser} 
			loading = {this.state.loading}
			sendFrgtPswVerificationCode = {this.sendFrgtPswVerificationCode}
			setNewPasswordUnauthenticated = {this.setNewPasswordUnauthenticated}
			registerUser = {this.registerUser}
			setNewPasswordForNewUser = {this.setNewPasswordForNewUser}
			showAllProjects = {this.showAllProjects}/>
		}
		else{
			pageContent = 
				<div>
					<Header
					user = {this.state.user}
					logoutUser = {this.logoutUser} 
					projects = {this.state.projects}
					openProject = {this.openProject}
					showAllProjects = {this.showAllProjects}
					createNewProject = {this.createNewProject}/>
					<Browser 
					user = {this.state.user}
					items={items}
					mode = {this.state.mode}
					loading = {this.state.loading}
					currentProject = {this.state.cProject.name}
					currentProjectUuid = {this.state.cProject.uuid}
					credentials = {this.state.credentials}
					newProject = {this.state.newProject}
					createNewProject = {this.createNewProject}
					removeSelectedProject = {this.removeSelectedProject}
					removeSelectedContent = {this.removeSelectedContent}
					editSelectedProjectName = {this.editSelectedProjectName}
					editCurrentProjectName = {this.editCurrentProjectName}
					editSelectedContentName = {this.editSelectedContentName}
					editCurrentContentName = {this.editCurrentContentName}
					openProject = {this.openProject}
					showAllProjects = {this.showAllProjects}
					loginUser = {this.loginUser}
					uploadPhotos = {this.uploadPhotos}
					uploadTour = {this.uploadTour}
					createContent = {this.createContent}
					currentContent = {this.state.currentContent}
					setCurrentContent = {this.setCurrentContent}
					newContent = {this.state.newContent}
					deactivateNewContent = {this.deactivateNewContent}
					loadingAfterUpload = {this.loadingAfterUpload}
					cProject = {this.state.cProject}
					cContent = {this.state.cContent}/>
				</div>
		}

		return (
			<div>
				{pageContent}
			</div>
		);
	}
});

module.exports = App;


