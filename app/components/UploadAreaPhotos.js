var React = require('react');
var FineUploaderS3 = require('react-fine-uploader/wrappers/s3');
var GalleryObj = require('react-fine-uploader/components/gallery');
var Gallery = GalleryObj.default;
var Config = require('../configs/mainConfig');

var user = "";
var currentProject = "";
var uploadedFiles;
var uploadingFiles;
var imageFormats = Config.upload.imageFormats;
var videoFormats = Config.upload.videoFormats;


const uploader = new FineUploaderS3.default({
    options: {
        request: {
            endpoint: Config.upload.userBucket,
        },
        objectProperties: {
            // Since we want all items to be publicly accessible w/out a server to return a signed URL
            acl: "public-read",
            key: function(id) {
                var filename = this.getName(id),
                    uuid = this.getUuid(id);
                    ext = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();

                if (!!~imageFormats.indexOf(ext)){
                    return uuid + '.' + ext;
                }
                else if (!!~videoFormats.indexOf(ext)){
                    return uuid + '.' + ext;
                }
                else return user + /trash/ + uuid + '.' + ext;
            },
        },
    },
});

var UploadAreaPhotos = React.createClass({
    getInitialState: function () {
    return {
            done: false,
        };
    },
    componentDidMount: function() { 
        uploader.methods.reset();
        uploader.on('validateBatch', this.onValidate);
        uploader.on('submitted', this.onSubmitted);
        uploader.on('complete', this.onComplete);
        uploader.on('cancel', this.onCancel);
        uploader.methods.setCredentials(this.props.credentials);
        user = this.props.user;
        currentProject = this.props.cProject.name;
        uploadingFiles = 0;
        uploadedFiles = 0;


    },

    onValidate: function(data, buttonContainer){
        
        
        var maxSize = Config.upload.maxFileSize;
        if (data.length+uploadedFiles+uploadingFiles > 1) {
            alert("Only 1 file can be uploaded at once.")
            return false;
        }
        var ext = data[0].name.substr(data[0].name.lastIndexOf('.') + 1).toLowerCase();
        var extValid = false;
        for (var j = 0; j < imageFormats.length; j++){
            if (ext == imageFormats[j]){
                uploader.methods.setEndpoint(Config.upload.userBucket)
                extValid = true;
                break;
            }
        }
        if (extValid == false){
            for (var j = 0; j < videoFormats.length; j++){
                if (ext == videoFormats[j]){
                    uploader.methods.setEndpoint(Config.upload.videosInBucket);
                    extValid = true;
                    break;
                }
            }
        }
        
        if (extValid == false){
            alert(ext + " is not a valid extension.")
            return false;
        }
        if (data[0].size > maxSize){
            alert(data[0].name + " is too big. Files can't be bigger than " + maxSize/1000000 + "Mb");
            return false;
        }
        return true;
    },

    onSubmitted: function(id, name){
        uploadingFiles++;
    },

    onCancel: function(id, name){
        uploadingFiles--;
    },


    onComplete: function(id, name, responseJSON, xhr){
        if (this.state.done == false){
            uploadingFiles--;
            uploadedFiles++;
            var filename = uploader.methods.getKey(id);
            var ext = filename.substr(filename.lastIndexOf('.') + 1);
            var file = filename.substr(filename.lastIndexOf('/') + 1);
            var title = name.substr(0, name.lastIndexOf('.'));

            if (!!~imageFormats.indexOf(ext)){
                this.props.createContent(title, "photos", [{Name: file}], this.props.cProject.uuid);
            }
            else if (!!~videoFormats.indexOf(ext)){
                this.props.createContent(title, "videos", [{Name: file}], this.props.cProject.uuid);
            }
            this.state.done = true;
            this.props.loadingAfterUpload();
        }
    },
	render: function(){
		return (    <div style = {{width: "100%"}}>
    					<Gallery 
                        dropzone-disabled = {false} 
                        uploader = {uploader} 
                        dropzone-text = "Arrastra y suelta los archivos aquí para subirlos"
                        dropzone-image = "https://s3-eu-west-1.amazonaws.com/libelios.web/static/app/img/Upload_Image.png"/>
                    </div>
			);
	}
});

module.exports = UploadAreaPhotos;