var React = require('react');
var FineUploaderS3 = require('react-fine-uploader/wrappers/s3');
var GalleryObj = require('react-fine-uploader/components/gallery');
var Gallery = GalleryObj.default;
var Config = require('../configs/mainConfig');

var user = "";
var currentProject = "";
var imageFormats = Config.upload.imageFormats;


const uploader = new FineUploaderS3.default({
    options: {
        request: {
            endpoint: Config.upload.userBucket,
        },
        objectProperties: {
            // Since we want all items to be publicly accessible w/out a server to return a signed URL
            acl: "public-read",
            key: function(id) {
                var filename = this.getName(id),
                    uuid = this.getUuid(id);
                    ext = filename.substr(filename.lastIndexOf('.') + 1);
                return uuid + '.' + ext;
            },
        },
    },
});

var UploadAreaPhotos = React.createClass({
    getInitialState: function () {
    return {
            done: false,
            files: [],
            defaultName: "",
        };
    },
    componentDidMount: function() { 
        uploader.methods.reset();
        uploader.on('complete', this.onComplete);
        uploader.on('allComplete', this.onAllComplete);
        uploader.on('validateBatch', this.onValidate);
        uploader.on('submitted', this.onSubmitted);
        uploader.on('cancel', this.onCancel);
        uploader.methods.setCredentials(this.props.credentials);
        user = this.props.user;
        currentProject = this.props.cProject.name;

    },

    onValidate: function(data, buttonContainer){
        
        
        var maxSize = Config.upload.maxFileSize;
        var ext = data[0].name.substr(data[0].name.lastIndexOf('.') + 1);
        var extValid = false;
        for (var j = 0; j < imageFormats.length; j++){
            if (ext == imageFormats[j]){
                extValid = true;
                break;
            }
        }
        if (extValid == false){
            alert(ext + " is not a valid extension.")
            return false;
        }
        if (data[0].size > maxSize){
            alert(data[0].name + " is too big. Files can't be bigger than " + maxSize/1000000 + "Mb");
            return false;
        }
        return true;
    },

    onComplete: function(id, name, responseJSON, xhr){
        if (this.state.defaultName == ""){
            this.state.defaultName = name.substr(0, name.lastIndexOf('.'));
        }
    },

    onSubmitted: function(id, name){
        this.setState({done: false})
    },

    onAllComplete: function(sucess, failed){
        for (var i = 0; i<sucess.length; i++){
            var filename = uploader.methods.getKey(sucess[i]);
            var file = filename.substr(filename.lastIndexOf('/') + 1);
            this.state.files.push({Name: file})
        }
        this.setState({done: true})   
    },

    onCancel: function(id, name){
        if (this.state.files.length > 0) this.setState({done: true});   
    },

    send: function(){
        this.props.createContent(this.state.defaultName, "tours", this.state.files, this.props.cProject.uuid);
        this.props.loadingAfterUpload();
    },
    render: function(){
        var sendButton = []
        if (this.state.done) sendButton = <input className="button nextButton" type="submit" value="Siguiente" onClick={this.send}/>;

        return (    <div style = {{width: "100%", position: "relative"}}>
                        <Gallery 
                        dropzone-disabled={false} 
                        uploader={uploader} 
                        dropzone-text = "Arrastra y suelta los archivos aquí para subirlos"
                        dropzone-image = "https://s3-eu-west-1.amazonaws.com/libelios.web/static/app/img/Upload_Image.png"/>
                        {sendButton}
                    </div>
            );
    }
});

module.exports = UploadAreaPhotos;