var React = require('react');
var Icon = require('./Icon')
var Loading = require('./Loading')


var Workspace = React.createClass({
	selectItem: function(e){
		this.props.selectItem(e.currentTarget.getAttribute('data-key'));
	},
	dblClick: function(e){
		if (this.props.mode == 0){
			this.props.openProject(e.currentTarget.getAttribute('data-key'));
		}
		if (this.props.mode == 1){
			this.props.openEditContent(e.currentTarget.getAttribute('data-key'));
		}
	},

	render: function(){
		var items = [];
		if(this.props.loading == false){
			//Create a list with all needed <Icon/> elements
		    for (var key in this.props.items) {
		      items.push(
		        <Icon 
		        selectItem = {this.selectItem}
		        items = {this.props.items}
		        dblClick = {this.dblClick}
		        key={key} 
		        num={key} 
		        selected={key==this.props.selectedID}/>
		      );
		    }
		}

	    //If no <Icon/> elements has been added to items, insert Add Project Button to "items".
	    //CHANGE NEEDED: Makes sense only for mode 0 (All Projects). In mode 1 (Project content) the user should be asked
	    //if he wants to add a Video, Photo or Tour instead of a Project.
	    if (items.length == 0 && this.props.mode == 0 && this.props.loading == false){
	    	var buttonText = "Crear " + (this.props.mode == 0?"proyecto":"elemento");
	    	items.push(
	    		<div id="no-item" key = {0}>
	    			<span>No hay ningún {this.props.mode == 0?"proyecto":"elemento"}</span>
	    			<br/><br/>
	    			<input type="submit" value = {buttonText} onClick={this.props.createNewProject}/>
	    		</div>
	        );
	    }
	    //If loading
	    if (this.props.loading)
	    	items.push(<Loading key = {0}/>);

		return (
				<div id="workspace">
					{items}
				</div>
			);
	}
});

module.exports = Workspace;