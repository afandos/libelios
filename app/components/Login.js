var React = require('react');
var CognitoManagment = require('../actions/CognitoManagment');
var Loading = require('./Loading');
var Config = require('../configs/mainConfig');


var Login = React.createClass({
	getInitialState: function () {
    return {
    		user: "",
    		email: "",
	      	pass: "",
	      	pass2: "",
	      	verification: "",
	      	error: "",
	      	show: true,
	      	loginmode: 0, //0 Login, 1 Registro, 2 Petición de recuperación contraseña, 3 Nueva contraseña recuperada, 4 Nueva contraseña nuevo usuario, 5 mensaje confirmación nuevo password/usuario
	    };
	},
	handleUserChange: function(event){
		this.setState({user: event.target.value});
	},
	handleEmailChange: function(event){
		this.setState({email: event.target.value});
	},
	handlePassChange: function(event){
		this.setState({pass: event.target.value});
	},
	handleSecondPassChange: function(event){
		this.setState({pass2: event.target.value});
	},
	handleVerificationChange: function(event){
		this.setState({verification: event.target.value});
	},
	handleLoginSubmit: function(event){
		event.preventDefault();
		this.props.loginUser(this.state.user, this.state.pass, this);
	},
	handleRegisterSubmit: function(event){
		event.preventDefault();
		
		if (this.state.pass == this.state.pass2) this.props.registerUser(this.state.email, this.state.user, this.state.pass, this);
		else alert("Las contraseñas no coinciden.")
	},
	handleRecoverSubmit: function(event){
		event.preventDefault();
		this.props.sendFrgtPswVerificationCode(this.state.user, this);
	},
	handleNewPassSubmit: function(event){
		event.preventDefault();
		if (this.state.pass == this.state.pass2) this.props.setNewPasswordUnauthenticated(this.state.user, this.state.verification, this.state.pass, this);
		else alert("Las contraseñas no coinciden.")
	},
	handleNewUserPassSubmit: function(event){
		event.preventDefault();
		if (this.state.pass == this.state.pass2) this.props.setNewPasswordForNewUser(this.state.user, this.state.pass, this);
		else alert("Las contraseñas no coinciden.")
	},

	test: function(event){
		this.setState({loginmode: 1});
	},

	goToRecoverPassword: function(){
		console.log("recover")
		this.setState({loginmode: 2});
	},
	goToSignUp: function(){
		console.log("sign up")
		this.setState({loginmode: 1});
	},
	goToSignIn: function(event){
		if (event != null) event.preventDefault();
		console.log("sign in")
		this.setState({loginmode: 0});
	},
	enter: function(event){
		event.preventDefault();
		this.props.showAllProjects();
	},
	render: function(){
		var showClass = "";
		if (this.state.show) showClass="active";
		else showClass ="inactive"; 

		var classVector = []
		for (i=0; i<6; i++){
			if (this.state.loginmode == i) classVector.push("active")
			else classVector.push("inactive")
		}

		


		var loadingForm = this.props.loading?
								<Loading/>:
								<div>
									<div className ={classVector[0]}>
										<form onSubmit={this.handleLoginSubmit}>
											<div>
										        <input type="text" onChange={this.handleUserChange} placeholder = "Username" />
										        <br/>
										        <input type="password" onChange={this.handlePassChange} placeholder = "Password"/>
									        </div>
									        <div className="loginDataBottom">
									        	<div className="loginError">{this.state.error}</div>
									        	<input type="submit" value="Iniciar sesión" />
									        </div>
									    </form>
								    </div>
							        <div className ={classVector[1]}>
								        <form onSubmit={this.handleRegisterSubmit}>
											<div>
										        <input type="text" onChange={this.handleUserChange} placeholder = "Username" />
										        <br/>
										        <input type="text" onChange={this.handleEmailChange} placeholder = "E-mail" />
										        <br/>
										        <input type="password" onChange={this.handlePassChange} placeholder = "Password"/>
										        <br/>
										        <input type="password" onChange={this.handleSecondPassChange} placeholder = "Repeat Password"/>
									        </div>
									        <div className="loginDataBottom">
									        	<div className="loginError">{this.state.error}</div>
									        	<input type="submit" value="Registrar" />
									        </div>
								        </form>
							        </div>
							        <div className ={classVector[2]}>
								        <form onSubmit={this.handleRecoverSubmit}>
											<div>
										        <input type="text" onChange={this.handleUserChange} placeholder = "Username or email" />
										        <br/>
									        </div>
									        <div className="loginDataBottom">
									        	<div className="loginError">{this.state.error}</div>
									        	<input type="submit" value="Recuperar cuenta" />
									        </div>
								        </form>
							        </div>
							        <div className ={classVector[3]}>
								        <form onSubmit={this.handleNewPassSubmit}>
											<div>
										        <input type="text" value={this.state.user} onChange={this.handleUserChange} placeholder = "Username or email" />
										        <br/>
										        <input type="text" onChange={this.handleVerificationChange} placeholder = "Verifiction code" />
										        <br/>
										        <input type="password" onChange={this.handlePassChange} placeholder = "Password"/>
										        <br/>
										        <input type="password" onChange={this.handleSecondPassChange} placeholder = "Repeat Password"/>
										        
									        </div>
									        <div className="loginDataBottom">
									        	<div className="loginError">{this.state.error}</div>
									        	<input type="submit" value="Confirmar contraseña" />
									        </div>
								        </form>
							        </div>
							        <div className ={classVector[4]}>
								        <form onSubmit={this.handleNewUserPassSubmit}>
											<div>
										        <input type="text" value={this.state.user} onChange={this.handleUserChange} placeholder = "Username or email" />
										        <br/>
										        <input type="password" onChange={this.handlePassChange} placeholder = "Password"/>
										        <br/>
										        <input type="password" onChange={this.handleSecondPassChange} placeholder = "Repeat Password"/>
										        
									        </div>
									        <div className="loginDataBottom">
									        	<div className="loginError">{this.state.error}</div>
									        	<input type="submit" value="Confirmar contraseña" />
									        </div>
								        </form>
							        </div>
							        <div className ={classVector[5]}>
								        <form onSubmit={this.goToSignIn}>
											<div>
										        <span>The new password has been set. Please log in with your new password.</span>
									        </div>
									        <div className="enterButton">
									        	<input type="submit" value="Entrar" />
									        </div>
								        </form>
							        </div>
						        </div>

		var signInOrSignUp = this.state.loginmode == 0 ? 
		<span className="loginCreate">¿No tienes cuenta? <span className="loginCreateLink" onClick={this.goToSignUp}>Regístrate.</span></span>:
		<span className="loginCreate">¿Ya tienes cuenta? <span className="loginCreateLink" onClick={this.goToSignIn}>Inicia sesión.</span></span>
		var loginRecover = this.state.loginmode == 2 ?
		<span className="loginRecover"></span>:
		<span className="loginRecover" onClick={this.goToRecoverPassword}>¿Se te ha olvidado el nombre de usuario o la contraseña?</span>

							    

		return(
				<div className="loginBackground">
					<div className="loginWindow">
						<div className="loginLogo">
							<div><img src={Config.login.logoUrl}/></div>
						</div>
						<div className="loginData">
								{loadingForm}
						</div>
						<div className="loginPlus">
							{loginRecover}
							<br/>
							{signInOrSignUp}
						</div>
				     </div>
				</div>
			);
		
	}
});

module.exports = Login;