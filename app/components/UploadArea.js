var React = require('react');
var FineUploaderS3 = require('react-fine-uploader/wrappers/s3');
var GalleryObj = require('react-fine-uploader/components/gallery');
var Gallery = GalleryObj.default;

var eDate = new Date(2018, 1, 1, 1, 1, 1, 1);


const uploader = new FineUploaderS3.default({
    options: {
        request: {
            endpoint: "http://libelios.users.s3.amazonaws.com",
        },
        credentials: {
            accessKey: "BSIAJSN6LFBDJOTFPKEA",
            secretKey: "gFBy/D/U4tXZrfYsW+d24lpOpqKgDBoSRVMKox4Z",
            sessionToken: "AgoGb3JpZ2luEFIaCWV1LXdlc3QtMSKAAjJxt+Ebax0bttwcu6acV5EvfVwRyCyEOX4Ki7njIa0rA/5o0UWn/GBsoIaylDHIK04G4CqjHMxxsZBFmYpSSpfCQJKsYmA6fpd9ZmC891XHnaFXMq/3Rv0dk5bWAqd41wpKJ2vd7+0xWtJjsm0hejdhgb+OFbSVE5J85iJJRSRabWTwEOsviBV6VRM5Q/iHjRrztpOfzyOeru4N0122KVb0szfBu5xL/C0kjPmAfcLZS+GMPZsX23xQSIlDxauFDLr97jPK+YGEB7L0FtDoe2Bncv/D5GOY/tiNvFzjeszPBd455s7ixkd1SBaybfUfAAz49pfBdEYyXbx/hG01kywqrwUIp///////////ARAAGgw0NDc0NzQ1NTYzNTEiDGuAmetBIGAZygGXqyqDBaTYEg4QHdconGxOPMN631nOjw+VThhfAgDsxBsM/1AhojNtzK3TBxyD+WagwDb81YWXt6VdedM+BmAQLD2KkgxEBL4VCJMXQMM19kefJa0MmUMRCry94tFai/n82Qk72IubFvlwj1kpXEVQrIre0s95VlI6Y/5Fqar7A8YGkJlqPXniG0rkaAgXtXDzjBvU/YSP9fGsHzBk/OphrAVZRHfgBJrHz2bsrX+X3J+FnZ5etTGIXJ2QSNPNdAmrAU7WQgxp+ZTm1W1myjK5FzWSpRVxxRvIOckSWuQ2PCvLz7c6BrSuegCJH6odm5fIz/HekcHbTmNjk5JpqJoTthP0HoSXAWPFll5eB/WlBFGusJz8HML+SAiuObw3LlRFtl3yS3beQZYsHQxxhwPYsTs0KzHc1h51c9fnK8h8vHjpwXUUV+3X3Z5GYcWZgP6OqwQS25OuwAr+lwS51RW7ZfF41DHCOmg2zN7QMEh9uj+oksXodlMFo8DSxXoBbymDKJp9X31bwiVWCYraYGGxawOWHDfjXApSGrDL1abLRciXeZZRzfNQ7LCqWYMR9na8Y7JIYPq52FT16cd2XvbxSxa6QCA8paj/iEOcH2YGUEuaRVsRzxdEmKI3DVfOWfgzW+EOGFJQ5vs+otRLVK00UnNsp1ozEO1aMLuuTgVZ8by2CivlnSmicEsmuUmADBaAVEXcvsnO6MrZ8LaxWRAuyJKXmN+ghaF+znXrRiumOorwZFMuXBSVU9sq4tX2sLIIHHZUs0KObqowZWDulSuvJ5f3rxBF3qVmm7YkACzkG/uALHyBO2iMHXSlZxoJzpMYH01PwoTWYaawv5n/j7n7vptLAceyDEQwxpzewwU=",
            expiration: eDate,
        },
        objectProperties: {
            // Since we want all items to be publicly accessible w/out a server to return a signed URL
            acl: "public-read",

            key: function(id) {
                var filename = this.getName(id),
                    uuid = this.getUuid(id);
                    ext = filename.substr(filename.lastIndexOf('.') + 1);
                return "kure127" + "/" + uuid + '.' + ext;
            },
        
        },
        validation: {
            itemLimit: 100,
            sizeLimit: 1000000000,
            allowedExtensions: ['jpg', 'JPG'],
        }
    },
});




var UploadArea = React.createClass({
    componentDidMount: function() { 
        uploader.on('complete', this.onComplete)

    },

    onComplete: function(id, name, responseJSON, xhr){
        var filename = uploader.methods.getKey(id);
        var ext = filename.substr(filename.lastIndexOf('.') + 1);

    },
	render: function(){
		return (    <div style = {{width: "100%"}}>
    					<Gallery 
                        dropzone-disabled={false} 
                        uploader={uploader} 
                        dropzone-text = "Arrastra y suelta los archivos aquí para subirlos"
                        dropzone-image = "https://s23.postimg.org/g12ovlcej/Upload_Image.png"/>
                    </div>
			);
	}
});

module.exports = UploadArea;