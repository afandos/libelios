var React = require('react');
var Sort = require('../actions/Sort');
var config = require('../configs/mainConfig');

var Header = React.createClass({
	render: function(){
		var centerImage = {
		    justifyContent: 'center',
		    alignItems: 'center',
		  }

		var sortedProjects =  Sort.byModificationDate(this.props.projects, true)//sortArr(this.props.Projects);
		var lastProjects = [];
		for (i=0; i<(this.props.projects.length < 3?sortedProjects.length:3); i++){
			lastProjects.push(
					<div key={i} className="header-unfoldable-menu-item header-sub-menu-item" onClick = {this.props.openProject.bind(null, 0, sortedProjects[i])}>
						{sortedProjects[i].name}
					</div>
				)
		}

		var menus = [];
		menus.push(
			<div key={1} className="header-menu-item">
				<div><img src={config.header.projects} style={{verticalAlign:"middle"}}/><span> Proyectos</span></div>
				<div className="header-unfoldable-menu-container-background">
					<div className="header-unfoldable-menu-container">
						<div>
							<div className="header-unfoldable-menu-item" onClick = {this.props.createNewProject}>
								Nuevo proyecto
							</div>
							<div className="header-unfoldable-menu-item" onClick={this.props.showAllProjects.bind(null, null)}>
								Todos los proyectos
							</div>
							{lastProjects}
						</div>
					</div>
				</div>
			</div>
			);
		menus.push(
			<div key={2} className="header-menu-item">
				<div><img src={config.header.help} style={{verticalAlign:"middle"}}/><span> Ayuda </span></div>
				<div className="header-unfoldable-menu-container-background">
					<div className="header-unfoldable-menu-container">
						<div>
							<div className="header-unfoldable-menu-item">
								Manual
							</div>
							<div className="header-unfoldable-menu-item">
								FAQ
							</div>
							<div className="header-unfoldable-menu-item">
								Sobre Libelios
							</div>
						</div>
					</div>
				</div>
			</div>
			);
		menus.push(
			<div key={3} className="header-menu-item">
				<div><img src={config.header.user} style={{verticalAlign:"middle"}}/><span className = "userName"> {this.props.user} </span></div>
				<div className="header-unfoldable-menu-container-background">
					<div className="header-unfoldable-menu-container">
						<div>
							<div className="header-unfoldable-menu-item">
								Nueva Organización
							</div>
							<div className="header-unfoldable-menu-item">
								Configuracion
							</div>
							<div className="header-unfoldable-menu-item" onClick={this.props.logoutUser}>
								Cerrar sesión
							</div>
						</div>
					</div>
				</div>
			</div>
			);



		return (
			<div id="header">
				<div id="header-logo">
					<img src={config.header.logo}/>
				</div>
				<div id="header-menu">
				 {menus}
				</div>
			</div>
			);
	}
});


function sortArr(arr){
	return arr.concat().sort(compare);
}

function compare(a,b){
	if (a.date < b.date)
		return 1;
	if (a.date > b.date)
		return -1;
	return 0;
}


module.exports = Header;