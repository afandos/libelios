var React = require('react');
var config = require('../configs/mainConfig');

var RadialButton = React.createClass({
	getInitialState: function () {
    	return {
            selected: null,
        };
    },

    componentWillMount: function(){
    	if (this.props.default != null) this.state.selected = this.props.default;
    },

    onClick: function(e){
    	if(this.state.selected == e.currentTarget.getAttribute('data-key')){
    		this.setState({selected : null});
    	}
    	else{
    		this.setState({selected : e.currentTarget.getAttribute('data-key')});
    	}
    	this.props.selectionChanged(e.currentTarget.getAttribute('data-key'))
    },

    render: function(){
    	var radialButtons = [];

    	for (var i = 0; i < this.props.captions.length; i++){
    		var src = (i == this.state.selected?config.radialButton.checked:config.radialButton.unchecked);

    		radialButtons.push(
    			<div key = {i}  className="radialButtonsUnit" >
    				<img src={src} data-key = {i} onClick={this.onClick}/><span>{this.props.captions[i]}</span>
    			</div>
    		);
    	}

    	return <div className="radialButtonsContainer">{radialButtons}</div>;

    },





	});

module.exports = RadialButton;