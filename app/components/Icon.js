var React = require('react');
var Config = require('../configs/mainConfig');


var Icon = React.createClass({
	render: function(){
		var imgURL = "";
		switch (this.props.items[this.props.num].type){
			case "project":
					imgURL = Config.icon.project;
					break;
			case "photos":
					imgURL = Config.icon.photo;
					break;
			case "tours":
					imgURL = Config.icon.tour;
					break;
			case "videos":
					imgURL = Config.icon.video;
					break;
		}
		var background = this.props.selected?{
			borderColor: "#261055",
			borderStyle: "dashed"
		}:{};

		var style = {
			filter: this.props.selected?"grayscale(0%)":"grayscale(100%) brightness(140%)"
		}
		return (
				<div id="icon-space">
					<div id="icon" data-key={this.props.num} style={background} onClick={this.props.selectItem} onDoubleClick={this.props.dblClick}>
						<div id="icon-content">
							<div className="iconImagePlaceholder"/>
							<span>{this.props.items[this.props.num].name}</span>
							<img id = "icon-image" src={imgURL} style={style}/>
						</div>
					</div>
				</div>
			);
	}
});

module.exports = Icon;