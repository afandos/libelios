var React = require('react');
var SearchBar = require('./SearchBar');

var BrowserTitle = React.createClass({
	navBack: function(){
		if (this.props.mode == 1){
			this.props.showAllProjects();
		}
		else if (this.props.mode == 2 || this.props.mode == 4){
			console.log(this.props)
			this.props.openProject(0,this.props.cProject);
		}

	},

	render: function(){

		if (this.props.mode == 0){
		var title = "Todos los proyectos";
		}
		if (this.props.mode == 1){
			var title = this.props.cProject.name;
		}
		if (this.props.mode == 2){
			var title = "Subir foto o video a " + this.props.cProject.name;
		}
		if (this.props.mode == 4){
			var title = "Subir tour a " + this.props.cProject.name;
		}
		var navBackButton = [];
		var edit = [];

		if (this.props.mode == 1 || this.props.mode == 2 || this.props.mode == 4){
			navBackButton.push(<div key={0} className="navBack"><div onClick={this.navBack}/></div>)
		}

		if (this.props.mode == 1){
			edit.push(<div key={1} id="browser-title-edit"><img src="https://s3-eu-west-1.amazonaws.com/libelios.web/static/app/img/editCurrentProjectName.png" onClick = {this.props.activateEditName.bind(null, false)}/></div>)
		}
		if (this.props.newProject) {
			edit.push(
				<div key={2} className="arrow-left-container">
					<div className="arrow-left"/>
				</div>);
			edit.push(
				<div key={3} className="information">
					Edita el nombre
				</div>);
		}
		var searchBar = [];
		if (this.props.mode == 0 || this.props.mode == 1){
			searchBar = <SearchBar 
					items={this.props.items}
					mode={this.props.mode}
					openProject={this.props.openProject}
					openEditContent = {this.props.openEditContent}
					/>
		}
		
		return (
				<div id="browser-title">
					{navBackButton}
					<div id="browser-title-text"><span>{title}</span></div>
					{edit}
					{searchBar}
					
				</div>
			);
	}
});

module.exports = BrowserTitle; 