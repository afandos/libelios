var React = require('react');
var RadialButtons = require('./RadialButtons');
var ApiGateway = require('../actions/ApiGateway');
var config = require('../configs/mainConfig');
var HelpFunctions = require('../actions/HelpFunctions');


var EditContent = React.createClass({
	getInitialState: function () {
    return {
	      	conf: {},
	      	vr: false,
	      	embedCode: "",
	      	photoProcessing: false,
	    };
	},

	componentWillMount: function(){
		if (this.props.cContent.type == "photos") ApiGateway.getPanoConfig(this, this.props.user, this.props.cContent.uuid)
		this.setState({vr: true})
	},

	loadConf: function(conf){
		if (this.props.cContent.state != 2){
			this.setState({photoProcessing: true});
		}
		else {
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = this.checkConf.bind(null, xhr, conf);
			xhr.open("GET", conf.xml, true);
			xhr.send();
		}
	},

	checkConf: function(xhr, conf){
		if (xhr.readyState == 4) {
			var isPhotoAvailable = false;
		    xml = HelpFunctions.xmlToJson(jQuery.parseXML(xhr.responseText));
		    if(!xml.hasOwnProperty("Error")) isPhotoAvailable = true;  
		    if (!isPhotoAvailable){
			}
			this.setState({conf: conf});
			if (isPhotoAvailable) {
				this.applyConf(conf);
				this.setState({photoProcessing: false});
			}
			else{
				this.setState({photoProcessing: true});
			}
		}
	},

	applyConf: function(conf){



		var vars = {};
		vars["plugin[vtoureditor].url"] = conf.plugin;
		vars["plugin[vtoureditor].keep"] = true;
		embedpano({swf: conf.swf, xml: conf.xml, target:"pano", flash:"only", passQueryParameters:false, vars:vars});
	},

	closeEditContent: function(e){
		if (e.target !== e.currentTarget){
			return;
		}
		removepano("pano");
		this.props.closeEditContent();
	},

	updatePlayerConfig: function(){
		ApiGateway.updatePlayerConfig(this, this.props.user, this.props.cContent.uuid, this.state.vr);
		this.setState({embedCode: '<iframe width="560" height="315" src="http://www.libelios.com/embedp?user=' + this.props.user + '&cuuid=' + this.props.cContent.uuid + '" frameborder="0" allowfullscreen></iframe>'})
	},


	render: function(){
		var defaultColor = ['ffffff', 'f8f9e1', 'f8eacc', 'f5e4ee', 'f1cce5', 'f2cecf', 'e0ced5', 'ecf3f0', 'cce9da', 'ccddd3', 'ecf5fc', 'e2e8f3', 'ccd2e5', 'eeeeee', 'cccccc']
		var defaultColorInput = [];
		var photoProcessing = this.state.photoProcessing?"Please wait, this photo is being processed. This can take several minutes":"";
		for (var i = 0; i < defaultColor.length; i++){
			defaultColorInput.push(
				<div key={i} className="defaultColorContainer">
					<div className="defaultColor" style = {{backgroundColor: '#' + defaultColor[i]}}/>
				</div>
			)
		}

		return (
				<div className="grayscreenSub" onClick={this.closeEditContent}>
					<div className="editContent">
						<div className="editContentTop">
							<div className="editContentTitle">
								<div><span>{this.props.cContent.name}</span></div>
								<div><img src={config.confContent.editIcon} onClick = {this.props.activateEditName.bind(null, false)}/></div>
							</div>
							<div className="editContentClose">
								<span onClick = {this.closeEditContent}>✖</span>
							</div>
						</div>
						<div className="editContentMiddle">
							<div className="editContentMiddleLeft">
								<div className="editContentMediaContainer">
									<div className="editContentMediaPlayer">
										<div id="pano" style={{width:"100%", height:"100%"}}>{photoProcessing}</div>
									</div>
									<div className="editContentMediaInfo">
									</div>
								</div>
							</div>
							<div className="editContentMiddleRight">
								<div className="editContentMiddleRightTopSpace"/>
								<div className="editContentSection">
									<div className="editContentNum">
										<span className="inactiveText">1.</span>
									</div>
									<div className="editContentDes">
										<div><span className="inactiveText">Añade un <span className="bold">icono</span> a tu foto:</span></div>
										<div className="confDefaultButtons"><input className="posButtonIn" type="submit" value="Subir icono..."/><input className="negButtonIn" type="submit" value="Quitar icono"/></div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										<span className="inactiveText">2.</span>
									</div>
									<div className="editContentDes">
										<span className="inactiveText"> Elija el color para la barra del reproductor: </span>
										<br/>
										<div className="defaultColors">
											{defaultColorInput}
										</div>
										<div className="colorCode">
											# <input type="text" disabled/>
										</div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										<span className="inactiveText">3.</span>
									</div>
									<div className="editContentDes">
										<span className="inactiveText">Personaliza el thumbnail:</span>
										<div className="confDefaultButtons"><input className="posButtonIn" type="submit" value="Subir imagen..." disabled/><input className="negButtonIn" type="submit" value="Generar imagen" disabled/></div>
									</div>
								</div>
								<div className="editContentSection">
									<div className="editContentNum">
										4.
									</div>
									<div className="editContentDes">
										Obten el <span className="bold">código embed</span>:
										<div className="embedGeneration">
											<div className="embedGenerationButton"><input className="corpButton" type="submit" value="GENERAR" onClick={this.updatePlayerConfig}/></div>
											<div className="embedGenerationDesc">Genera el código embed y guarda<br/> la nueva configuración.</div>
										</div>
										<div className="embedGet">
											<input type="text" value={this.state.embedCode}/><input type="submit" value="copiar"/>
										</div>
									</div>		
								</div>
							</div>
						</div>
					</div>
				</div>
			);
	}
});

module.exports = EditContent;


