var React = require('react');
var SearchItem = require('./SearchItem');
var config = require('../configs/mainConfig');


var SearchBar = React.createClass({
	getInitialState: function () {
    return {
	      	results: [],
	      	searchTerm: "",
	    };
	},

	openResult: function(e){
		console.log(this.state.results[e.currentTarget.getAttribute('data-key')]);
		if(this.props.mode == 0){
			this.props.openProject(null, this.state.results[e.currentTarget.getAttribute('data-key')] )
		}
		else if(this.props.mode == 1){
			this.props.openEditContent(null, this.state.results[e.currentTarget.getAttribute('data-key')]);
		}
		this.setState({searchTerm: ""})
		this.setState({results :  [] });
	},


	search: function(){
		if(this.state.searchTerm == "")
			this.setState({results :  [] });
		else
			this.setState({results :  this.props.items.filter(startsWith,this.state.searchTerm.toLowerCase()) });
		this.forceUpdate();
	},

	onChange: function(e){
		this.state.searchTerm = e.target.value;
		this.search();
	},

	render: function(){
		var results = [];
		for (i=0; i<(this.state.results.length < 10?this.state.results.length:10); i++){
			results.push(
				<div key={i} data-key={i} onClick={this.openResult}>
					<SearchItem  name={this.state.results[i].name} />
				</div>
			);
		};



		return (
				<div id="searchbar-contaianer">
					<div>
						<input type="text" id="searchbar-input" onClick = {this.search} onChange = {this.onChange} value = {this.state.searchTerm}/>
						<div id="search-results">
							{results}
						</div>
					</div>
					<div className="searchbar-icon">
						<img src={config.searchBar.icon} width = "17px" height = "23px" style={{padding: "1px"}}/>
					</div>
					
				</div>
			);
	}
});

module.exports = SearchBar;


function startsWith(element){
		return element.name.toLowerCase().startsWith(this);
}