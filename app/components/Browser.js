var React = require('react');
var BrowserTitle = require('./BrowserTitle');
var Menubar = require('./Menubar');
var Workspace = require('./Workspace');
var ChangeName = require('./ChangeName');
var RemoveConfirmation = require('./RemoveConfirmation');
var VideoEditContent = require('./VideoEditContent')
var PhotoEditContent = require('./PhotoEditContent')
var TourEditContent = require('./PhotoEditContent')
var UploadAreaTour = require('./UploadAreaTour');
var UploadAreaPhotos = require('./UploadAreaPhotos');
var Loading = require('./Loading')



var Browser = React.createClass({
	getInitialState: function () {
    return {
	      	Items: [],
	      	selected: false,
	      	selectedID: null,
	      	editName: false,
	      	editVideoContent: false,
	      	editPhotoContent: false,
	      	editTourContent: false,
	      	removeItem: false,
	      	selectedNotCurrent: true,
	      	lastMode: 0,
	      	lastProject: {},
	    };
	},

	componentWillReceiveProps: function(){
		if (this.props.mode != this.state.lastMode || this.props.cProject != this.state.lastProject){
			this.state.lastProject = this.props.cProject;
			this.state.lastMode = this.props.mode;
			this.setState({selected: false});
			this.setState({selectedID: null});
		}
	},

	componentWillUpdate: function (){
		if (this.props.newContent) {
			if (this.props.cContent.type == "videos") this.setState({editVideoContent: true})
			if (this.props.cContent.type == "photos") this.setState({editPhotoContent: true})
			if (this.props.cContent.type == "tours") this.setState({editTourContent: true})	
			this.props.deactivateNewContent();
		}
	},

	addItem: function(){
		this.props.createNewProject();
	},

	activateRemoveItem: function(){
		this.setState({removeItem: true});
	},
	deactivateRemoveItem: function(){
		this.setState({removeItem: false});
	},

	removeSelectedProject: function(){
		if (this.state.selected){
			if (this.props.mode == 0)
				this.props.removeSelectedProject(this.state.selectedID);
			else if (this.props.mode == 1)
				this.props.removeSelectedContent(this.state.selectedID);
			this.setState({selectedID: null});
			this.setState({selected: false})
			this.setState({removeItem: false});
		}
	},

	activateEditName: function(selectedNotCurrent){
		this.setState({selectedNotCurrent: selectedNotCurrent})
		this.setState({editName: true});
	},
	deactivateEditName: function(){
		this.setState({editName: false});
	},
	editName: function(newName){
		if (this.state.selectedNotCurrent && this.props.mode == 0)
			this.props.editSelectedProjectName(newName, this.state.selectedID);

		else if (!this.state.selectedNotCurrent && this.props.mode == 1 && (!this.state.editVideoContent && !this.state.editPhotoContent))
			this.props.editCurrentProjectName(newName);

		else if (this.state.selectedNotCurrent && this.props.mode == 1)
			this.props.editSelectedContentName(newName, this.state.selectedID);

		else if (!this.state.selectedNotCurrent && this.props.mode == 1 && (this.state.editVideoContent || this.state.editPhotoContent))
			this.props.editCurrentContentName(newName);

		this.setState({editName: false});
	},
	selectItem: function(id){
		if(this.state.selectedID == id){
			this.setState({selectedID: null});
			this.setState({selected: false})
		}
		else{
			this.setState({selectedID: id});
			this.setState({selected: true})
		}
		
	},

	openProject: function(id, project){
		this.setState({selectedID: null});
		this.setState({selected: false})
		this.props.openProject(id, project);
	},

	openEditContent: function(id ,item){
		this.props.setCurrentContent(id, item);
		if (item == null){
			if (this.props.items[id].type == "videos") this.setState({editVideoContent: true})
			if (this.props.items[id].type == "photos") this.setState({editPhotoContent: true})	
			if (this.props.items[id].type == "tours") this.setState({editTourContent: true})
		}
		else {
			if (this.props.cContent.type == "videos") this.setState({editVideoContent: true})
			if (this.props.cContent.type == "photos") this.setState({editPhotoContent: true})	
			if (this.props.cContent.type == "tours") this.setState({editTourContent: true})	
		}

	},

	closeEditContent: function(id){
		this.props.openProject(null, this.props.cProject);
		this.setState({editVideoContent: false})
		this.setState({editPhotoContent: false})
		this.setState({editTourContent: false})
		this.props.deactivateNewContent();
	},


	render: function(){
		
		

		if (this.state.editName) var currentName = this.state.selectedNotCurrent?this.props.items[this.state.selectedID].name:((this.state.editVideoContent||this.state.editPhotoContent||this.state.editTourContent)?this.props.cContent.name:this.props.cProject.name);
		var editNamePopUp = this.state.editName?<ChangeName 
												key={1} 
												currentName = {currentName}
												selectedNotCurrent = {this.state.selectedNotCurrent}
												editName = {this.editName}
												deactivateEditName = {this.deactivateEditName}
												/>:null;

		var removeItemPopUp = this.state.removeItem?<RemoveConfirmation 
													key={2} 
													removeSelectedProject = {this.removeSelectedProject}
													deactivateRemoveItem = {this.deactivateRemoveItem}
													selectedID = {this.state.selectedID}
													items = {this.props.items}
													/>:null;
		var editVideoContentPopUp = this.state.editVideoContent?<VideoEditContent key={3} 
														closeEditContent = {this.closeEditContent}
														cContent = {this.props.cContent}
														activateEditName = {this.activateEditName}
														cContent = {this.props.cContent}
														cProject = {this.props.cProject}
														user = {this.props.user}/>:null;

		var editPhotoContentPopUp = this.state.editPhotoContent?<PhotoEditContent key={3} 
														closeEditContent = {this.closeEditContent}
														cContent = {this.props.cContent}
														activateEditName = {this.activateEditName}
														cContent = {this.props.cContent}
														cProject = {this.props.cProject}
														user = {this.props.user}/>:null;
		var editTourContentPopUp = this.state.editTourContent?<TourEditContent key={4} 
														closeEditContent = {this.closeEditContent}
														cContent = {this.props.cContent}
														activateEditName = {this.activateEditName}
														cContent = {this.props.cContent}
														cProject = {this.props.cProject}
														user = {this.props.user}/>:null;

		var display = <div/>;

		var browserTitle = <BrowserTitle 
							openProject = {this.props.openProject}
							mode = {this.props.mode}
							newProject = {this.props.newProject}
							items = {this.props.items}
							activateEditName = {this.activateEditName}
							showAllProjects = {this.props.showAllProjects}
							cProject = {this.props.cProject}
							/>
		if (this.props.mode == 0 || this.props.mode == 1){
			display = <div>
						<div id="mock-browser-title"/>
						<div id="browser-under-title">
							<Menubar 
							mode = {this.props.mode}
							selected = {this.state.selected}
							createNewProject = {this.props.createNewProject}
							activateEditName = {this.activateEditName}
							activateRemoveItem = {this.activateRemoveItem}
							uploadPhotos = {this.props.uploadPhotos}
							uploadTour = {this.props.uploadTour}/>

							<Workspace 	
							selectItem = {this.selectItem}
							mode = {this.props.mode}
							openProject = {this.openProject}
							loading = {this.props.loading}
							items = {this.props.items}
							selectedID = {this.state.selectedID}
							createNewProject = {this.props.createNewProject}
							openEditContent = {this.openEditContent}/>

							{browserTitle}
						</div>
						{editNamePopUp}
						{removeItemPopUp}
						{editVideoContentPopUp}
						{editPhotoContentPopUp}
						{editTourContentPopUp}
					</div>
		}
		else if (this.props.mode == 2){
			var content = this.props.loading?<Loading/>:<UploadAreaPhotos 
				credentials = {this.props.credentials} 
				user = {this.props.user} 
				createContent = {this.props.createContent}
				openProject = {this.props.openProject}
				cProject = {this.props.cProject}
				loadingAfterUpload = {this.props.loadingAfterUpload}/>
			display = <div>
						<div id="mock-browser-title"/>
						<div id="browser-under-title">
							{content}
							{browserTitle}
						</div>
					</div>
		}
		else if (this.props.mode == 3){

		}
		else if (this.props.mode == 4){
			var content = this.props.loading?<Loading/>:<UploadAreaTour 
			credentials = {this.props.credentials} 
			user = {this.props.user} 
			createContent = {this.props.createContent}
			openProject = {this.props.openProject}
			cProject = {this.props.cProject}
			loadingAfterUpload = {this.props.loadingAfterUpload}/>
			display = <div>
						<div id="mock-browser-title"/>
						<div id="browser-under-title">
							{content}
							{browserTitle}
						</div>
					</div>
		}

		return (
			<div id="content">
				<div id="browser">
					{display}
				</div>
			</div>
		);
	}
});

module.exports = Browser;