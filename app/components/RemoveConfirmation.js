var React = require('react');
var config = require('../configs/mainConfig');

var RemoveConfirmation = React.createClass({

	deactivateRemoveItem: function(e){
		if (e.target !== e.currentTarget){
			return;
		}
		this.props.deactivateRemoveItem();
	},

	render: function(){
		return (
				<div className="grayscreen" onClick={this.deactivateRemoveItem}>
					<div className="popup">
						<span>¿Seguro que desea eliminar {this.props.items[this.props.selectedID].name}?</span><br/>
						<img src={config.removeElement.icon} height="23px" width="34px"/>
						<input className="delete button" type="submit" value="Aceptar" onClick={this.props.removeSelectedProject}/>
						<input className="send button" type="submit" value="Cancelar" onClick={this.props.deactivateRemoveItem}/>
						
					</div>
				</div>
			);
	}
});

module.exports = RemoveConfirmation;