var mainConfig = {
	upload : {
		imageFormats : ["jpg", "jpeg"],
		videoFormats : ["3gp", "mp4",  "avi",  "wmv", "webm"],
		userBucket : "http://libelios-photosin.s3.amazonaws.com",
		videosInBucket : "http://libelios-videosin.s3.amazonaws.com",
		maxFileSize : 1000000000,
	},
	icon : {
		project : "http://www.libelios.com/app/img/folder-color.png",
		photo : "http://www.libelios.com/app/img/photo-color.png",
		tour : "http://www.libelios.com/app/img/tour-color.png",
		video : "http://www.libelios.com/app/img/video-color.png",
	},
	login : {
		logoUrl : "http://www.libelios.com/app/img/logo-login.png",
		identityPoolId : "eu-west-1_S7VJKQznM",
		userPoolId : "eu-west-1_S7VJKQznM",
		appClientId : "2dlpfkdhai09aim0gutj7tti0v",
		region : "eu-west-1",
		credentials : {
			identityPoolId : 'eu-west-1:0be20141-4183-4c27-9329-21bf24d85e04',
			loginUrl : 'cognito-idp.eu-west-1.amazonaws.com/eu-west-1_S7VJKQznM', //has to be changed manually in "CognitoManagment.js"
		}
		
	},
	menubar : {
		newProject: {
			active: "http://www.libelios.com/app/img/menu-newProject.png",
			inactive: "",
		},
		changeName: {
			active: "http://www.libelios.com/app/img/menu-changeName.png",
			inactive: "http://www.libelios.com/app/img/menu-changeName-blocked.png",
		},
		remove: {
			active: "http://www.libelios.com/app/img/menu-erase.png",
			inactive: "http://www.libelios.com/app/img/menu-erase-blocked.png",
		},
		upload: {
			active: "http://www.libelios.com/app/img/menu-upload.png",
			inactive: "",
		},
		newTour: {
			active: "http://www.libelios.com/app/img/menu-newTour.png",
			inactive: "",
		},
	},
	radialButton : {
		checked: "http://www.libelios.com/app/img/radChecked.png",
		unchecked: "http://www.libelios.com/app/img/radUnchecked.png",
	},
	removeElement : {
		icon: "http://www.libelios.com/app/img/eliminando_carpeta_azul.png"
	},
	changeName : {
		icon: "http://www.libelios.com/app/img/mini_folder_green.png",
	},
	searchBar : {
		icon: "http://www.libelios.com/app/img/search.png",
	},
	header : {
		logo: "http://www.libelios.com/app/img/LogoTopLeft.png",
		projects: "http://www.libelios.com/app/img/header-project.jpg",
		help: "http://www.libelios.com/app/img/header-help.jpg",
		user: "http://www.libelios.com/app/img/user+logo.png"
	},
	confContent : {
		editIcon: "http://www.libelios.com/app/img/pencil.png",
	}
	
}

module.exports = mainConfig;