 var config = require('../configs/mainConfig');

 // App specific
var identityPoolId = config.login.identityPoolId;
var userPoolId = config.login.userPoolId;
var appClientId = config.login.appClientId;
var region = config.login.region;

// constructed
var loginId = 'cognito-idp.' + region + '.amazonaws.com/' + userPoolId;
var poolData = {
   UserPoolId: userPoolId,
   ClientId: appClientId
};


AWS.config.region = 'eu-west-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
	IdentityPoolId: identityPoolId // your identity pool id here
});

AWSCognito.config.region = 'eu-west-1';
AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	IdentityPoolId: identityPoolId // your identity pool id here
});


var CognitoManagment = {
	loginUser: function(username, password, App, LoginComp){
		// Need to provide placeholder keys unless unauthorised user access is enabled for user pool
	   AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'});
	   var cognitoUser = getCognitoUserFromUsername(username);
	   var authenticationData = {
	      Username: username,
	      Password: password
	   };
	   var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
		cognitoUser.authenticateUser(authenticationDetails, {
		    onSuccess: function(result) {
		        App.setState({loading: false});
		        App.setState({user: cognitoUser.username})
		        App.showAllProjects();
		    },
		    onFailure: function(err) {
		        LoginComp.setState({error: err.message})
		        App.setState({loading: false});
		    },
		    newPasswordRequired: function(userAttributes, requiredAttributes) {
	            // User was signed up by an admin and must provide new 
	            // password and required attributes, if any, to complete 
	            // authentication.

	            // TODO REQUIRE NEW PASSWORD
	            App.setState({loading: false});
	            LoginComp.setState({loginmode: 4});
	            LoginComp.setState({userAttributes: userAttributes});
	            LoginComp.setState({cognitoUser: cognitoUser});
	            

	            //alert("creating new password: 12345678")
	            // the api doesn't accept this field back

	            //var newPassword = "12345678"
	  
	            //CognitoManagment.setNewPasswordForNewUser(cognitoUser, newPassword, userAttributes, App)
	        }
		});
	},

	getCurrentUser: function()
	{
	   var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	   var cognitoUser = userPool.getCurrentUser();
	   return cognitoUser;
	},

	checkSessionValidity: function(cognitoUser, App){
		//AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'});
		console.log("Checking Session Validity:")
		console.log("Cognito User:")
		console.log(cognitoUser)
		cognitoUser.getSession(function(err, session) {
            if (err) {
               App.logoutUser()
            }
            if (!session.isValid()){
            	App.logoutUser();
            }
        }
        )
	},

	logoutUser: function()
	{
	   var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
	   var cognitoUser = userPool.getCurrentUser();
	   if (cognitoUser != null) cognitoUser.signOut();
	},

	getCredentials: function(cognitoUser, App){
		cognitoUser.getSession(function(err, session){
	        if (err) {
	           console.log(err)
	        }
	        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
			    IdentityPoolId: config.login.credentials.identityPoolId,
			    Logins: {'cognito-idp.eu-west-1.amazonaws.com/eu-west-1_S7VJKQznM': session.getIdToken().getJwtToken()}
			});
			AWS.config.credentials.get(function(){
			    // Credentials will be available when this function is called.
			    credentials = {
			    	accessKey : AWS.config.credentials.accessKeyId,
			    	secretKey : AWS.config.credentials.secretAccessKey,
			    	sessionToken : AWS.config.credentials.sessionToken,
			    	expiration: AWS.config.credentials.expireTime,
			    }
			    App.setState({credentials: credentials});
			    App.setState({loading: false});
			});
	    });
	},

	registerUser: function(email, username, password, App, LoginComp){

	    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

	    var attributeList = [];

	    var dataEmail = {
	        Name : 'email',
	        Value : email
	    };

	    var attributeEmail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail);

	    attributeList.push(attributeEmail);

	    userPool.signUp(username, password, attributeList, null, function(err, result){
	        if (err) {
	            alert(err);
	            return;
	        }
	        console.log("Please check your email");
	        App.setState({loading: false});
	        LoginComp.setState({loginmode: 0})
	        //cognitoUser = result.user;
	        //console.log('user name is ' + cognitoUser.getUsername());
	    });
	},

	confirmUser: function(username, verificationCode){
	AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'});
	var cognitoUser = getCognitoUserFromUsername(username);
	console.log(cognitoUser)
    cognitoUser.confirmRegistration(verificationCode, true, function(err, result) {
        if (err) {
            alert(err);
            return;
        }
        console.log('call result: ' + result);
    });
	},

	resendConfirmationCode: function(username){
		var cognitoUser = getCognitoUserFromUsername(username);

		cognitoUser.resendConfirmationCode(function(err, result) {
	        if (err) {
	            alert(err);
	            return;
	        }
	        console.log('call result: ' + result);
	    });
	},

	sendFrgtPswVerificationCode: function(username, App, LoginComp){
	    var cognitoUser = getCognitoUserFromUsername(username);

	    // call forgotPassword on cognitoUser
	    cognitoUser.forgotPassword({
	        onSuccess: function(result) {
	            console.log('call result: ' + result);
	            App.setState({loading: false});
	            LoginComp.setState({loginmode: 3})
	        },
	        onFailure: function(err) {
	            alert(err);
	            App.setState({loading: false});
	        },
	        inputVerificationCode: function() { 
	            App.setState({loading: false});
	            LoginComp.setState({loginmode: 3})
	        }
	    });
	},

	setNewPasswordUnauthenticated: function(username, verificationCode, newPassword, App, LoginComp){

		var cognitoUser = getCognitoUserFromUsername(username);

	    return new Promise(function(resolve, reject) {
	        cognitoUser.confirmPassword(verificationCode, newPassword, {
	            onFailure: function(err) {
	                alert(err);
	                App.setState({loading: false});
	                reject(err)
	            },
	            onSuccess: function() {
	            	alert("The new password has been set. Please login.")
	                App.setState({loading: false});
	            	LoginComp.setState({loginmode: 0})
	            	resolve();
	            },
	        });
	    });
	},

	setNewPasswordAuthenticated: function(username, oldPassword, newPassword){
		var cognitoUser = getCognitoUserFromUsername(username);

		cognitoUser.changePassword(oldPassword, newPassword, function(err, result) {
	        if (err) {
	            alert(err);
	            return;
	        }
	        console.log('call result: ' + result);
	    });
	},

	setNewPasswordForNewUser: function(username, newPassword, App, LoginComp){
		var cognitoUser = LoginComp.state.cognitoUser;
		var userAttributes = LoginComp.state.userAttributes;
	    delete userAttributes.email_verified;
        
        // Get these details and call 
        cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, {
            onFailure: function(err) {
                alert(err);
            },
            onSuccess: function() {
                App.setState({loading: false});
                App.setState({user: cognitoUser.username})
	        	LoginComp.setState({loginmode: 5})
            },
        });
	},
}



function getCognitoUserFromUsername(username){
	var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

	var userData = {
	    Username: username,
	    Pool: userPool
    };
    var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
    return cognitoUser;
}

module.exports = CognitoManagment;