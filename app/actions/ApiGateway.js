var createProject = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/project/create';

var getProject = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/project/manage/get';
var updateProjectName = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/project/manage/update';
var deleteProject = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/project/manage/delete';


var getContent = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/manage/get';
var deleteContent = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/manage/delete';
var updateContentName = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/manage/update';

var databaseCreate = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/create/db-item';
var photosConvert = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/create/pano';
var videosConvert = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/create/video';

var getPlayerConfig = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/conf/get/player';
var updatePlayerConfig ='https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/conf/update/player';

var getPanoConfig = 'https://5b7u2dyuc8.execute-api.eu-west-1.amazonaws.com/beta/content/conf/get/pano';


var Sort = require('./Sort');


var ApiGateway = {
	getProject: function(comp, user){
		
		var data = {
    		user: user,
  		}
		$.ajax({
	    	type: 'POST',
	    	url: getProject,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		comp.state.projects.length = 0;
	      		var eobj = JSON.parse(e);
	      		for (var i = 0; i<eobj.Projects.length; i++){
	      			comp.state.projects.push(
	      				{
	      					name: eobj.Projects[i].projectName, 
	      					date: 23131231+i, 
	      					creationDate: eobj.Projects[i].creationDate,
	      					updateDate: eobj.Projects[i].updateDate,
	      					type: "project", 
	      					uuid: eobj.Projects[i].projectUuid
	      				});
	      		}
	      		comp.state.projects = Sort.byName(comp.state.projects, true);
	      		comp.setState({loading: false});
			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	createProject: function(comp, user){
		
		var data = {
    		user: user,
  		}
  		comp.state.content.length = 0;
		$.ajax({
	    	type: 'POST',
	    	url: createProject,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	      		var eobj = JSON.parse(e);
	      		comp.state.projects.push(
	      			{
	      				name: eobj.projectName, 
	      				creationDate: eobj.creationDate,
	      				updateDate: eobj.updateDate,
	      				type: "project", 
	      				uuid: eobj.projectUuid
	      			});
	      		comp.setState(
	      			{
	      			cProject:
	      				{
			      			name: eobj.projectName, 
		      				creationDate: eobj.creationDate,
		      				updateDate: eobj.updateDate,
		      				type: "project", 
		      				uuid: eobj.projectUuid
			      		}
			      	}
	      		)
	      		comp.setState({mode: 1});
	      		comp.setState({loading: false});
	      		comp.setState({newProject: true});

			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	updateProjectName: function(comp, user, uuid, name){
		var data = {
    		user: user,
    		projectUuid: uuid,
    		projectNewName: name,
  		}
		$.ajax({
	    	type: 'POST',
	    	url: updateProjectName,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		comp.state.projects.length = 0;
	      		var eobj = JSON.parse(e);
	      		for (var i = 0; i<eobj.Projects.length; i++){
	      			comp.state.projects.push(
	      				{
	      					name: eobj.Projects[i].projectName,
	      					creationDate: eobj.Projects[i].creationDate,
	      					updateDate: eobj.Projects[i].updateDate,
	      					type: "project", 
	      					uuid: eobj.Projects[i].projectUuid
	      				});
	      		}
	      		comp.state.projects = Sort.byName(comp.state.projects, true);
	      		comp.setState({loading: false});
			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	deleteProject: function(comp, user, uuid){
		var data = {
    		user: user,
    		projectUuid: uuid,
  		}
		$.ajax({
	    	type: 'POST',
	    	url: deleteProject,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		comp.state.projects.length = 0;
	      		var eobj = JSON.parse(e);
	      		for (var i = 0; i<eobj.Projects.length; i++){
	      			comp.state.projects.push(
	      				{
	      					name: eobj.Projects[i].projectName, 
	      					creationDate: eobj.Projects[i].creationDate,
	      					updateDate: eobj.Projects[i].updateDate, 
	      					type: "project", 
	      					uuid: eobj.Projects[i].projectUuid
	      				});
	      		}
	      		comp.state.projects = Sort.byName(comp.state.projects, true);
	      		comp.setState({loading: false});
			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	getContent: function(comp, user, uuid){
		var data = {
    		user: user,
    		projectUuid: uuid,
    		contentUuid: null
  		}
		$.ajax({
	    	type: 'POST',
	    	url: getContent,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		comp.state.content.length = 0;
	      		var eobj = JSON.parse(e);
	      		for (var i = 0; i<eobj.Content.length; i++){
	      			comp.state.content.push({
	      				name: eobj.Content[i].contentName, 
	  					creationDate: eobj.Content[i].creationDate,
	  					updateDate: eobj.Content[i].updateDate, 
	  					type: eobj.Content[i].contentType,
	  					uuid: eobj.Content[i].contentUuid,
	  					vr: eobj.Content[i].vr,
	  					state: eobj.Content[i].contentState
	      			});
	      		}
	      		comp.state.content = Sort.byName(comp.state.content, true);
	      		comp.setState({loading: false});
			},
	    	error: function (e) {
	    		alert("An error ocurred. Couldn't load content.");
	    		comp.showAllProjects();
	      	}
	  	})
	},

	databaseCreate: function(comp, user, uuid, name, type, files){
		var data = {
    		user: user,
    		contentProject: uuid,
    		contentName: name,
    		contentType: type,
    		inputNameFiles: files
  		}
		$.ajax({
	    	type: 'POST',
	    	url: databaseCreate,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		var eobj = JSON.parse(e);
	    		comp.setState({currentContentUuid: eobj.contentUuid,});
	    		comp.setState({currentContent: eobj.contentName});
	    		comp.setState(
	    					{cContent: 
	    						{	
	    							name: eobj.contentName,
	  								creationDate: eobj.creationDate,
									updateDate: eobj.updateDate, 
									type: eobj.contentType,
									uuid: eobj.contentUuid,
	  								vr: eobj.vr,
	  								state: eobj.contentState
								}
	  						});
	    		if (type == "photos" || type == "tours"){
	    			comp.startPhotosConversion();
	    		} else if (type == "videos"){
	    			comp.startVideoConversion();
	    		}
	    		comp.openProject(0, comp.state.cProject, true)
			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	updateContentName: function(comp, user, puuid, cuuid, name){
		var data = {
    		user: user,
    		contentUuid: cuuid,
    		contentNewName: name,
    		contentProject: puuid
  		}
  		$.ajax({
	    	type: 'POST',
	    	url: updateContentName,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		comp.state.content.length = 0;
	      		var eobj = JSON.parse(e);
	      		for (var i = 0; i<eobj.Content.length; i++){
	      			comp.state.content.push(
	      				{
	      					name: eobj.Content[i].contentName, 
		  					date: 23131231+i,
		  					creationDate: eobj.Content[i].creationDate,
		  					updateDate: eobj.Content[i].updateDate, 
		  					type: eobj.Content[i].contentType,
		  					uuid: eobj.Content[i].contentUuid,
	  						vr: eobj.Content[i].vr,
	  						state: eobj.Content[i].contentState

	      				});
	      		}
	      		comp.state.content = Sort.byName(comp.state.content, true);
	      		comp.setState({loading: false});
			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	deleteContent: function(comp, user, puuid, cuuid, type){
		var data = {
    		user: user,
    		projectUuid: puuid,
    		contentUuid: cuuid,
    		contentType: type
  		}
		$.ajax({
	    	type: 'POST',
	    	url: deleteContent,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		console.log(e)
	    		
	    		comp.state.content.length = 0;
	      		var eobj = JSON.parse(e);
	      		for (var i = 0; i<eobj.Content.length; i++){
	      			comp.state.content.push(
	      				{
	      					name: eobj.Content[i].contentName, 
		  					creationDate: eobj.Content[i].creationDate,
		  					updateDate: eobj.Content[i].updateDate, 
		  					type: eobj.Content[i].contentType,
		  					uuid: eobj.Content[i].contentUuid,
	  						vr: eobj.Content[i].vr,
	  						state: eobj.Content[i].contentState
	      				});
	      		}
	      		comp.state.content = Sort.byName(comp.state.content, true);
	      		comp.setState({loading: false});
			},
	    	error: function (e) {
	    		console.log(e);
	      	}
	  	})
	},

	photosConvert: function(comp, user, uuid){
		var data = {
    		user: user,
    		contentUuid: uuid,
  		}
		$.ajax({
	    	type: 'POST',
	    	url: photosConvert,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
			},
	    	error: function (e) {
	    		console.log(e)
	    		console.log("An error ocurred with photo conversion");
	      	}
	  	})
	},

	videosConvert: function(comp, user, cuuid){
		var data = {
    		user: user,
    		contentUuid: cuuid,
  		}
		$.ajax({
	    	type: 'POST',
	    	url: videosConvert,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
			},
	    	error: function (e) {
	    		console.log("An error ocurred with photo conversion");
	      	}
	  	})
	},

	getPlayerConfig: function(comp, user, cuuid){
		var data = {
    		user: user,
    		contentUuid: cuuid,
    		isMac: (navigator.appVersion.indexOf("Mac")!=-1),
  		}
  		$.ajax({
	    	type: 'POST',
	    	url: getPlayerConfig,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		eobj = JSON.parse(e);
	    		comp.loadConf(eobj)
			},
	    	error: function (e) {
	    		console.log("Couldn't obtain player configuation");
	      	}
	  	})
	},


	updatePlayerConfig: function(comp, user, cuuid, vr){
		var data = {
    		user: user,
    		contentUuid: cuuid,
    		vr: vr,
  		}
  		$.ajax({
	    	type: 'POST',
	    	url: updatePlayerConfig,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
			},
	    	error: function (e) {
	    		console.log("Couldn't obtain player configuation");
	      	}
	  	})
	},


	getPanoConfig: function(comp, user, cuuid){
		var data = {
    		user: user,
    		contentUuid: cuuid,
  		}
  		console.log(data);
  		$.ajax({
	    	type: 'POST',
	    	url: getPanoConfig,
	    	dataType: 'json',
	    	contentType: 'application/json',
	    	data: JSON.stringify(data),
	    	success: function (e) {
	    		console.log(e)
	    		eobj = JSON.parse(e);
	    		comp.loadConf(eobj)
			},
	    	error: function (e) {
	    		console.log("Couldn't obtain player configuation");
	      	}
	  	})
	},
}

module.exports = ApiGateway;