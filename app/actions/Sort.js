var Sort = {
	byCreationDate: function(arr, asc){
		return arr.concat().sort(compareByCreationDate(asc));
	},
	byModificationDate: function(arr, asc){
		return arr.concat().sort(compareByModificationDate(asc));
	},
	byName: function(arr, asc){
		return arr.concat().sort(compareByName(asc));
	},
	byType: function(arr, asc){
		return arr.concat().sort(compareByType(asc));
	},
}

function compareByCreationDate(asc){
	return function(a,b) {
		aD = new Date(a.creationDate);
		bD = new Date(b.creationDate);
		if (aD < bD){
			return asc?1:-1;
		}
		if (aD > bD){
			return asc?-1:1;
		}
		return 0;
	}
}

function compareByModificationDate(asc){
	return function(a,b) {
		aD = new Date(a.updateDate);
		bD = new Date(b.updateDate);
		if (aD < bD){
			return asc?1:-1;
		}
		if (aD > bD){
			return asc?-1:1;
		}
		return 0;
	}
}

function compareByName(asc){
	return function(a,b) {
		return asc?a.name.localeCompare(b.name):-a.name.localeCompare(b.name);
	}
}

function compareByType(asc){
	return function(a,b) {
		if (a.date < b.date)
			return 1;
		if (a.date > b.date)
			return -1;
		return 0;
	}
}

module.exports = Sort;