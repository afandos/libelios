var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./components/App');
require("./css/styles.css");


$( document ).ready(function() {
    ReactDOM.render(<App />, document.getElementById('app'));
});
